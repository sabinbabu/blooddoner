package com.binwin.blooddoner

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.title = "Home" // changing text of toolbar

        button.setOnClickListener {
            if (spinner.selectedItemPosition > 0) {
                val intent = Intent(this, DonerListActivity::class.java)
                intent.putExtra("bloodGroup", spinner.selectedItem.toString())
                if (address.text.isNotEmpty()) intent.putExtra("address", address.text.toString())
                this.startActivity(intent)
            } else {
                Toast.makeText(this, "Please select blood group", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
