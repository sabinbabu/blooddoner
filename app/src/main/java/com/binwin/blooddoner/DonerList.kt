package com.binwin.blooddoner

data class DonerList(var full_name: String, var address: String, var blood_group: String, var contact: String)
