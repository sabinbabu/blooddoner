package com.binwin.blooddoner

import android.app.Activity
import android.content.Intent
import android.net.Uri
import java.io.IOException
import java.io.InputStream


fun inputStreamToString(inputStream: InputStream): String? {
    try {
        val bytes = ByteArray(inputStream.available())
        inputStream.read(bytes, 0, bytes.size)
        return String(bytes)
    } catch (e: IOException) {
        return null
    }

}

fun makeCall(callNumber: String, activity: Activity) {
    val phoneIntent = Intent(Intent.ACTION_DIAL)
    phoneIntent.data = Uri.parse("tel:" + callNumber)
    activity.startActivity(phoneIntent)
}