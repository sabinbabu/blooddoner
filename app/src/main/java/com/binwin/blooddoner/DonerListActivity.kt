package com.binwin.blooddoner

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_doner_list.*


class DonerListActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doner_list)

        supportActionBar?.title = "Doner List"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)


        //reading from selected json file
        val myJson = inputStreamToString(this.resources.openRawResource(R.raw.doner_data))
        //converting jsonArray to array
        val myModel = Gson().fromJson(myJson, Array<DonerList>::class.java)

        //filtering according to blood group
        var filteredList = myModel.filter { it.blood_group.toUpperCase() == intent.getStringExtra("bloodGroup") }

        //filtering according to address(if any)
        if (intent.hasExtra("address")) {
            val addressFilter =
                filteredList.filter { it.address.toLowerCase() == intent.getStringExtra("address").toLowerCase() }
            filteredList = addressFilter
        }

        //displaying data to recycler view
        if (filteredList.isNotEmpty()) {
            noInfoText.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            val mAdapter = DonerRecyclerAdapter(this, filteredList.toList())
            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = mAdapter
        } else {
            noInfoText.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
            else -> { }
        }
        return true
    }
}

