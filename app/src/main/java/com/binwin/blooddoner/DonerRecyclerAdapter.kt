package com.binwin.blooddoner

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.custom_view_doner.view.*

class DonerRecyclerAdapter(val activity: Activity, private val items: List<DonerList>) :
    RecyclerView.Adapter<DonerRecyclerAdapter.RowViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        RowViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_view_doner, parent, false))

    override fun onBindViewHolder(viewHolder: RowViewHolder, position: Int) =
        viewHolder.bind(items[viewHolder.adapterPosition])

    override fun getItemCount() = items.size

    inner class RowViewHolder(viewGroup: View) : RecyclerView.ViewHolder(viewGroup) {
        fun bind(item: DonerList) {
            if (item.full_name == "") itemView.name.text = "(no name)"
            else itemView.name.text = item.full_name.toUpperCase()
            itemView.mobileNumber.text = item.contact
            itemView.blood_group.text = item.blood_group.toUpperCase()
            itemView.address.text = item.address

            itemView.relativeLayoutParent.setOnClickListener {
                makeCall(item.contact, activity)
            }
        }
    }
}